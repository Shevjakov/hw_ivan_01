import UIKit


//1. Константы, переменные и типы данных

/*1) Напишите переменные и константы всех базовых типов данных: int, UInt, float,
 double, string. У чисел вывести их минимальные и максимальные значения.*/

let hours: UInt8 = 24
let minutesInHour = 60
var days = 256
var heightOfEverest = 8848
let byte: UInt = 8
var kilobyte: UInt = 1024
var megabyte: UInt = 1_048_576

print(Int.min)
print(Int.max)
print(Int64.min)
print(Int64.max)
print(Int32.min)
print(Int32.max)
print(Int16.min)
print(Int16.max)
print(Int8.min)
print(Int8.max)

print(UInt.min)
print(UInt.max)
print(UInt64.min)
print(UInt64.max)
print(UInt32.min)
print(UInt32.max)
print(UInt16.min)
print(UInt16.max)
print(UInt8.min)
print(UInt8.max)

let piFloat: Float = 3.141_592_653_589_793_238
print(piFloat)

let piDouble = 3.141_592_653_589_793_238
print(piDouble)

Double.greatestFiniteMagnitude
Float.greatestFiniteMagnitude

var name = "Mike"
let stringExample = "Hello " + (name) + "!"
print(name)
print(stringExample)

/*2) Создайте список товаров с различными характеристиками (количество, название).
 Используйте typealias.*/


typealias frutsTotal = UInt
let oranges: frutsTotal = 80
let apples: UInt = 40
let melons: UInt = 10
let sum: frutsTotal = oranges + apples + melons
apples
oranges
melons
print(sum)

/*3) Напишите различные выражения с приведением типа.*/

var intExample: Int = 512
var uintExemple: UInt = 8848
var iAmSleeping: Bool = false
var floatExemple: Float = 3.141_592_653_589_793_238
var doubleExemple: Double = 3.141_592_653_589_793_238
let stringExemple: String = "Wake up!"

/* 4) Вычисления с операторами (умножение, деление, сложение, вычитание)
 Результат вычислений должен выводиться в консоль в таком виде: «3 + 2 = 5».*/

let a1 = 3
let b1 = 2
let c1 = a1 + b1
let d1 = a1 - b1
let e1 = a1 * b1
let f1 = a1 / b1
let g1 = a1 % b1
print( a1, "+", b1, " =", c1)
print( a1, "-", b1, " =", d1)
print( a1, "*", b1, " =", e1)
print( a1, "/", b1, " =", f1)
print( a1, "%", b1, " =", g1)

/*5) Declare two constants a and b of type Double and assign both a value.
 Calculate the average of a and b and store the result in a constant named average.*/

let a: Double = 2.2
let b: Double = 4.4
let average: Double = (a + b) / 2

/*6) A temperature expressed in °C can be converted to °F by multiplying by 1.8 then
 incrementing by 32. In this challenge, do the reverse: convert a temperature from °F
 to °C. Declare a constant named fahrenheit of type Double and assign it a value.
 Calculate the corresponding temperature in °C and store the result
 in a constant named celcius.*/

// °F = ( °C * 1.8 ) + 32
// °C = (°F - 32)/ 1.8
//let fahrenheit = (26*1.8)+32
//let celcius = (80-32)/1.8

let fahrenheit: Double = 80
let celcius = ( fahrenheit - 32) / 1.8
print(celcius)

/* 7) A circle is made up of 2𝜋 radians, corresponding with 360 degrees. Declare a
 constant degrees of type Double and assign it an initial value. Calculate the
 corresponding angle in radians and store the result in a constant named radians.*/

//angle in degrees = angle in radians * (180 / pi)
//angle in radians = angle in degrees * ( pi / 180)

let degrees: Double = 90
let radians = 90 * ( 3.14 / 180 )
print(radians)

/* 8) Declare four constants named x1, y1, x2 and y2 of type Double. These constants
 represent the 2-dimensional coordinates of two points. Calculate the distance between
 these two points and store the result in a constant named distance.*/

let x1: Double = 1.0
let y1: Double = 1.0
let x2: Double = 4.0
let y2: Double = 4.0

//distance = sqrt(pow(x2-x1,2)+pow(y2-y1,2))

let distance = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2))
print(distance)

//2. Строки

/*1) Напишите с помощью строк своё резюме: имя, фамилия, возраст, где живете, хобби и
 т.п.*/
let myName = "Ivan"
let mySurname = "Shevjakov"
var myAge = 34
var myCity = "Minsk"
let myNativeLanguage = "russian"
var myHobby = "travel"

/*2) Соберите из этих строк 1 большую (вспоминаем интерполяцию) и выведите в консоль.*/

var myCV = "\( myName) \( mySurname), \( myAge), \( myCity), \( myNativeLanguage), \( myHobby)."
print(myCV)

/*3) Напишите 10 строк, соберите их с помощью интерполяции и поставьте в нужных местах
 переносы на новую строку и пробелы (см. \n и \t).*/

let str0 = "first"
var str1 = "Some random text,"
var str2 = "complitly random"
var str3 = "just filling strings"
let str4 = "42"
let str5 = "or maybe 24"
var str6 = "default"
var str7 = "random"
var str8 = "absolutly"
var str9 = "last"

let bigStr = "\(str0) \(str1) \n \(str2)\n \(str3) \n \(str4) \t \(str5) \n \(str6)  \(str7) \n \(str8) \(str9)"
print(bigStr)

/*4) Разбейте собственное имя на символы, перенося каждую букву на новую строку.*/

for character in "Ivan" {
    print(character)
}

/*5) Создайте переменную типа Int и переменную типа String.
 Тип Int преобразуйте в String и с помощью бинарного оператора сложите 2 переменные
 в одну строку.*/
let volume1 = 80
let volume2 = "db"
let musicVolume = String(volume1) + volume2
print(musicVolume)

/*6) Create a string constant called firstName and initialize it to your first name.
 Also create a string constant called lastName and initialize it to your last name.*/

let firstName: String = "Ivan"
let lastName: String = "Shevjakov"

/*7) Create a string constant called fullName by adding the firstName and lastName
 constants together, separated by a space.*/

let fullName: String = firstName + " " + lastName
print(fullName)

/* 8) Using interpolation, create a string constant called myDetails that uses the
 fullName constant to create a string introducing yourself. For example, my string
 would read: "Hello, my name is Matt Galloway.".*/

let myDetails: String = "Hello, my name is \(fullName)"
print(myDetails)

/*3. Tuples*/

//1) Создать кортеж с 3-5 значениями. Вывести их в консоль 3 способами.

let redColor = "Red"
let greenColor = "Green"
let blueColor = "Blue"
("Red", "Green", "Blue")

let color = ("R", "G", "B")
color.0
color.1
color.2

let deviceName = (model:"IPad Pro", storage:"512gb", screenSize:"11 inch",housingColor:"Silver")
deviceName.model
deviceName.storage
deviceName.housingColor
deviceName.screenSize

/*2) Давайте представим, что мы сотрудник ГАИ и у нас есть какое-то количество
 нарушителей. Задача. Создать кортеж с тремя параметрами:
 - первый - превышение скорости: количество пойманных;
 - второй - вождение нетрезвым: количество пойманных;
 - третий - бесправники: количество пойманных.
 Распечатайте наших бедокуров в консоль через print().*/

let tiket = (80, 8, 35)
let (speedLimit, drunkDriving, noLicense) = tiket
print(tiket)

/*3) Выведите каждый параметр в консоль. Тремя способами.*/
print(tiket.0, tiket.1, tiket.2)

tiket.0
tiket.1
tiket.2

speedLimit
drunkDriving
noLicense

/*4) Создайте второй кортеж — нашего напарника. Значения задайте другие.*/

let tiket2 = (40, 5, 80)
let (speedLimit2, drunkDriving2, noLicense2) = tiket2
print(tiket2)
tiket2.1

/*5) Пусть напарники соревнуются: создайте третий кортеж, который содержит
 в себе разницу между первым и вторым.*/

//let tiketDiference = (tiket, tiket2)/2

/*6) Declare a constant tuple that contains three Int values followed by a Double.
 Use this to represent a date (month, day, year) followed by an average
 temperature for that date.*/

let dateMonth: Int = 06
let dateDay: Int = 27
let dateYear: Int = 2019
let dateAverageTemperature: Double = 26.2
let dateTuple = (dateMonth, dateDay, dateYear, dateAverageTemperature)
print(dateTuple)

dateYear
dateMonth
dateDay
dateAverageTemperature

/*7) Change the tuple to name the constituent components. Give them names related to
 the data that they contain: month, day, year and averageTemperature.*/

/*8) In one line, read the day and average temperature values into two constants.
 You’ll need to employ the underscore to ignore the month and year.*/

let (_, dDay, _, dAverTemp) = dateTuple
print(dDay, dAverTemp)

/*9) Up until now, you’ve only seen constant tuples. But you can create variable
 tuples, too. Change the tuple you created in the exercises above to a variable by
 using var instead of let. Now change the average temperature to a new value.*/






