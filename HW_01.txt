import UIKit

/*
1. Константы, переменные и типы данных



1) Напишите переменные и константы всех базовых типов данных: int, UInt, float, double, string. У чисел вывести их минимальные и максимальные значения.
2) Создайте список товаров с различными характеристиками (количество, название). Используйте typealias.
3) Напишите различные выражения с приведением типа.
4) Вычисления с операторами (умножение, деление, сложение, вычитание)
Результат вычислений должен выводиться в консоль в таком виде: «3 + 2 = 5».
5) Declare two constants a and b of type Double and assign both a value. Calculate the average of a and b and store the result in a constant named average.
6) A temperature expressed in °C can be converted to °F by multiplying by 1.8 then incrementing by 32. In this challenge, do the reverse: convert a temperature from °F to °C. Declare a constant named fahrenheit
of type Double and assign it a value. Calculate the corresponding temperature in °C and store the result in a constant named celcius.
7) A circle is made up of 2𝜋 radians, corresponding with 360 degrees. Declare a constant degrees of type Double and assign it an initial value. Calculate the corresponding angle in radians and store the result in a constant named radians.
8) Declare four constants named x1, y1, x2 and y2 of type Double. These constants represent the 2-dimensional coordinates of two points. Calculate the distance between these two points and store the result in a constant named distance.
*/


/*
2. Строки


1) Напишите с помощью строк своё резюме: имя, фамилия, возраст, где живете, хобби и т.п. 2) Соберите из этих строк 1 большую (вспоминаем интерполяцию) и выведите в консоль.
3) Напишите 10 строк, соберите их с помощью интерполяции и поставьте в нужных местах переносы на новую строку и пробелы (см. \n и \t).
4) Разбейте собственное имя на символы, перенося каждую букву на новую строку.
5) Создайте переменную типа Int и переменную типа String.
Тип Int преобразуйте в String и с помощью бинарного оператора сложите 2 переменные в одну строку.
6) Create a string constant called firstName and initialize it to your first name. Also create a string constant called lastName and initialize it to your last name.
7) Create a string constant called fullName by adding the firstName and lastName constants together, separated by a space.
8) Using interpolation, create a string constant called myDetails that uses the fullName constant to create a string introducing yourself. For example, my string would read: "Hello, my name is Matt Galloway.".
*/


/*
3. Tuples


1) Создать кортеж с 3-5 значениями. Вывести их в консоль 3 способами.
2) Давайте представим, что мы сотрудник ГАИ и у нас есть какое-то количество нарушителей. Задача. Создать кортеж с тремя параметрами:
- первый - превышение скорости: количество пойманных;
- второй - вождение нетрезвым: количество пойманных;
- третий - бесправники: количество пойманных.
Распечатайте наших бедокуров в консоль через print().
3) Выведите каждый параметр в консоль. Тремя способами.
4) Создайте второй кортеж — нашего напарника. Значения задайте другие.
5) Пусть напарники соревнуются: создайте третий кортеж, который содержит в себе разницу между первым и вторым.
6) Declare a constant tuple that contains three Int values followed by a Double. Use this to represent a date (month, day, year) followed by an average temperature for that date.
    7) Change the tuple to name the constituent components. Give them names related to the data that they contain: month, day, year and averageTemperature.
    8) In one line, read the day and average temperature values into two constants. You’ll need to employ the underscore to ignore the month and year.
    9) Up until now, you’ve only seen constant tuples. But you can create variable tuples, too. Change the tuple you created in the exercises above to a variable by using var instead of let. Now change the average temperature to a new value.
*/
